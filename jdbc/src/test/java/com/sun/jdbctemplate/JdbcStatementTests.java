package com.sun.jdbctemplate;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.text.SimpleDateFormat;

public class JdbcStatementTests {

    public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询
     */
    @Test
    public void testQuery() throws Exception {
        /**
         * 1. 注册驱动
         * com.mysql.jdbc.Driver：mysql-connector-java 5.x及以下
         * com.mysql.cj.jdbc.Driver：mysql-connector-java 6.x及以上
         */
        Class.forName("com.mysql.cj.jdbc.Driver");

        // 2.获取链接
        /**
         * serverTimezone：表示时区
         * 北京时间-GMT%2B8、上海时间-Asia/Shanghai（使用夏令时）、世界统一时间-UTC（比北京早8个小时）
         * useUnicode：使用Unicode编码
         * characterEncoding：字符的解码和编码的格式
         * useSSL：false-不使用ssl协议、true-使用ssl协议
         * SSL协议提供服务主要：
         *        1）认证用户服务器，确保数据发送到正确的服务器；
         *        2）加密数据，防止数据传输途中被窃取使用；
         *        3）维护数据完整性，验证数据在传输过程中是否丢失；
         *    当前支持SSL协议两层：
         *    	 	SSL记录协议（SSL Record Protocol）：建立靠传输协议（TCP）高层协议提供数据封装、压缩、加密等基本功能支持
         * 	    SSL握手协议（SSL Handshake Protocol）：建立SSL记录协议用于实际数据传输始前通讯双进行身份认证、协商加密
         * 	    算法、 交换加密密钥等。
         * rewriteBatchedStatements：允许批量插入
         * allowMultiQueries：允许批量更新
         */
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, "root", "123456");

        // 3.定义sql
        String sql = "SELECT * FROM user";

        // 4.获取执行sql的对象 statement
        Statement stmt = conn.createStatement();

        // 5.执行sql
        ResultSet resultSet = stmt.executeQuery(sql);

        // 6.处理结果
        while (resultSet.next()) {
            System.out.println(resultSet.getString(1) + " " + resultSet.getString(2)
                    + " " + resultSet.getString(3) + " " + resultSet.getString(4));
        }

        // 7.释放资源
        resultSet.close();
        stmt.close();
        conn.close();
    }

    /**
     * 单条插入
     */
    @Test
    public void testInsert() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        Statement statement = connection.createStatement();

        String now = sdf.format(new java.util.Date());
        int count = statement.executeUpdate("INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES('易思琪', 20, 'yisiqi@sun.com', '" + now + "', 'system', '" + now + "', 'system', 'N')");
        System.out.println(count);

        statement.close();
        connection.close();
    }

    /**
     * 批量插入
     */
    @Test
    public void testBatchInsert() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        // 关闭事务自动提交
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();
        // 当前时间
        String now = sdf.format(new java.util.Date());
        String sql1 = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES('白晴雪', 19, 'baiqingxue@sun.com', '" + now + "', 'system', '" + now + "', 'system', 'N')";
        String sql2 = "INSERT INTO orm.user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES('李琴心', 20, 'liqingxin@sun.com', '" + now + "', 'system', '" + now + "', 'system', 'N')";
        // 添加批量sql
        statement.addBatch(sql1);
        statement.addBatch(sql2);
        // 统一进行执行
        statement.executeBatch();
        // 清空
        statement.clearBatch();
        // 手动提交
        connection.commit();
        // 关闭
        statement.close();
        connection.close();
    }

    /**
     * 更新
     */
    @Test
    public void testUpdate() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        Statement statement = connection.createStatement();

        int count = statement.executeUpdate("UPDATE user SET age=21, email='yisiqi21@sun.com' WHERE name = '易思琪'");
        System.out.println(count);

        statement.close();
        connection.close();
    }

    /**
     * 删除
     */
    @Test
    public void testDelete() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");
        Statement statement = connection.createStatement();

        int count = statement.executeUpdate("DELETE FROM user WHERE name = '易思琪' OR name = '白晴雪' OR name = '李琴心'");
        System.out.println(count);

        statement.close();
        connection.close();
    }

}
