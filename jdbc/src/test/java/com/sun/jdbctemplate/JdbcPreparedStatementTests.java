package com.sun.jdbctemplate;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JdbcPreparedStatementTests {

    public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询
     */
    @Test
    public void testQuery() throws Exception {
        /**
         * 1. 注册驱动
         * com.mysql.jdbc.Driver：mysql-connector-java 5.x及以下
         * com.mysql.cj.jdbc.Driver：mysql-connector-java 6.x及以上
         */
        Class.forName("com.mysql.cj.jdbc.Driver");

        // 2.获取链接
        /**
         * serverTimezone：表示时区
         * 北京时间-GMT%2B8、上海时间-Asia/Shanghai（使用夏令时）、世界统一时间-UTC（比北京早8个小时）
         * useUnicode：使用Unicode编码
         * characterEncoding：字符的解码和编码的格式
         * useSSL：false-不使用ssl协议、true-使用ssl协议
         * SSL协议提供服务主要：
         *        1）认证用户服务器，确保数据发送到正确的服务器；
         *        2）加密数据，防止数据传输途中被窃取使用；
         *        3）维护数据完整性，验证数据在传输过程中是否丢失；
         *    当前支持SSL协议两层：
         *    	 	SSL记录协议（SSL Record Protocol）：建立靠传输协议（TCP）高层协议提供数据封装、压缩、加密等基本功能支持
         * 	    SSL握手协议（SSL Handshake Protocol）：建立SSL记录协议用于实际数据传输始前通讯双进行身份认证、协商加密
         * 	    算法、 交换加密密钥等。
         * rewriteBatchedStatements：允许批量插入
         * allowMultiQueries：允许批量更新
         */
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection conn = DriverManager.getConnection(url, "root", "123456");

        // 3.定义sql
        String sql = "SELECT * FROM user";

        // 4.获取执行sql的对象 PreparedStatement
        PreparedStatement ps = conn.prepareStatement(sql);

        // 5.执行sql
        ResultSet resultSet = ps.executeQuery(sql);

        // 6.处理结果
        while (resultSet.next()) {
            System.out.println(resultSet.getString(1) + " " + resultSet.getString(2)
                    + " " + resultSet.getString(3) + " " + resultSet.getString(4));
        }

        // 7.释放资源
        resultSet.close();
        ps.close();
        conn.close();
    }

    /**
     * 单条插入
     */
    @Test
    public void testInsert() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        // 预编译sql
        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        // 填充数据
        String now = sdf.format(new java.util.Date());
        ps.setString(1, "澄心语");
        ps.setInt(2, 21);
        ps.setString(3, "chengxinyu@sun.com");
        ps.setString(4, now);
        ps.setString(5, "system");
        ps.setString(6, now);
        ps.setString(7, "system");
        ps.setString(8, "N");
        // 执行sql语句
        ps.execute();
        // 关闭
        ps.close();
        connection.close();
    }

    /**
     * 批量插入
     */
    @Test
    public void testBatchInsert() throws Exception {
        long start = System.currentTimeMillis();
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false&rewriteBatchedStatements=true";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        String now = sdf.format(new Date());

        int batchSize = 10;
        for (int i = 1; i <= batchSize; i++) {
            // 填充数据
            ps.setString(1, "test" + i);
            ps.setInt(2, 22);
            ps.setString(3, "test" + i + "@test.com");
            ps.setString(4, now);
            ps.setString(5, "system");
            ps.setString(6, now);
            ps.setString(7, "system");
            ps.setString(8, "N");
            // 将sql语句打包到一个容器中
            ps.addBatch();
            if (i % 500 == 0) {
                // 将容器中的sql语句提交
                ps.executeBatch();
                // 清空容器，为下一次打包做准备
                ps.clearBatch();
            }
        }
        // 为防止有sql语句漏提交【如i结束时%500！=0的情况】，需再次提交sql语句
        // 将容器中的sql语句提交
        ps.executeBatch();
        // 清空容器
        ps.clearBatch();
        // 关闭
        ps.close();
        connection.close();
        System.out.println(batchSize + "条数据插入用时：" + (System.currentTimeMillis() - start)+"毫秒");
    }

    /**
     * 更新
     */
    @Test
    public void testUpdate() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        // 预编译sql
        String sql = "UPDATE user SET age = ?, email = ? WHERE name = ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        // 填充数据
        ps.setInt(1, 22);
        ps.setString(2, "chengxinyu22@sun.com");
        ps.setString(3, "澄心语");
        // 执行sql语句
        ps.executeUpdate();
        // 关闭
        ps.close();
        connection.close();
    }

    /**
     * 删除
     */
    @Test
    public void testDelete() throws Exception {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        // 预编译sql
        String sql = "DELETE FROM user WHERE name LIKE ? '%'";
        PreparedStatement ps = connection.prepareStatement(sql);
        // 填充数据
        ps.setString(1, "test");
        // 执行sql语句
        ps.executeUpdate();
        // 关闭
        ps.close();
        connection.close();
    }

}
