package com.sun.jdbctemplate;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 批量保存性能测试
 */
public class JdbcBatchTests {

    public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * 批量提交大小
     * 设置建议：
     * 设置适当的批处理大小：批处理大小指在一次插入操作中插入多少行数据。
     * 如果批处理大小太小，插入操作的频率将很高，而如果批处理大小太大，可能会导致内存占用过高。
     * 通常，建议将批处理大小设置为1000-5000行，这将减少插入操作的频率并降低内存占用。
     */
    private static final int BATCH_SIZE = 1000;

    /**
     * 方式一：普通插入
     *
     * 测试结果：
     * 1000条数据插入用时：1544毫秒
     * 10000条数据插入用时：15634毫秒
     * 100000条数据插入用时：157126毫秒
     */
    @Test
    public void testPreparedStatementInsert() throws Exception {
        long start = System.currentTimeMillis();
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);

        int total = 10000;
        String now = sdf.format(new Date());
        for (int i = 1; i <= total; i++) {
            // 填充数据
            ps.setString(1, "test" + i);
            ps.setInt(2, 22);
            ps.setString(3, "test" + i + "@test.com");
            ps.setString(4, now);
            ps.setString(5, "system");
            ps.setString(6, now);
            ps.setString(7, "system");
            ps.setString(8, "N");
            // 执行sql语句
            ps.execute();
        }
        ps.close();
        connection.close();
        System.out.println(total + "条数据插入用时：" + (System.currentTimeMillis() - start)+"毫秒");
    }

    /**
     * 方式二：使用批处理插入-设置数据源的批处理重写标志
     * 注意：通过连接配置url设置&rewriteBatchedStatements=true，打开驱动的rewriteBatchedStatements 开关
     *
     * 测试结果：（批处理大小为500）
     * 1000条数据插入用时：299毫秒
     * 1万条数据插入用时：498毫秒
     * 10万条数据插入用时：2187毫秒
     * 100万条数据插入用时：16680毫秒
     */
    @Test
    public void testPreparedStatementBatch() throws Exception {
        long start = System.currentTimeMillis();
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false&rewriteBatchedStatements=true";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        String now = sdf.format(new Date());

        int total = 1000000;
        for (int i = 1; i <= total; i++) {
            // 填充数据
            ps.setString(1, "test" + i);
            ps.setInt(2, 22);
            ps.setString(3, "test" + i + "@test.com");
            ps.setString(4, now);
            ps.setString(5, "system");
            ps.setString(6, now);
            ps.setString(7, "system");
            ps.setString(8, "N");
            // 将sql语句打包到一个容器中
            ps.addBatch();
            if (i % BATCH_SIZE == 0) {
                // 将容器中的sql语句提交
                ps.executeBatch();
                // 清空容器，为下一次打包做准备
                ps.clearBatch();
            }
        }
        // 为防止有sql语句漏提交【如i结束时%500！=0的情况】，需再次提交sql语句
        // 将容器中的sql语句提交
        ps.executeBatch();
        // 清空容器
        ps.clearBatch();
        // 关闭
        ps.close();
        connection.close();
        System.out.println(total + "条数据插入用时：" + (System.currentTimeMillis() - start)+"毫秒");
    }

    /**
     * 方式三：通过数据库连接取消自动提交，手动提交数据
     *
     * 测试结果：（批处理大小为500）
     * 1000条数据插入用时：348毫秒
     * 1万条数据插入用时：512毫秒
     * 10万条数据插入用时：1786毫秒
     * 100万条数据插入用时：12582毫秒
     *
     * 测试结果：（批处理大小为1000）
     * 1万条数据插入用时：480毫秒
     * 10万条数据插入用时：1844毫秒
     * 100万条数据插入用时：14032毫秒
     */
    @Test
    public void testPreparedStatementBatchCommit() throws Exception {
        long start = System.currentTimeMillis();
        Class.forName("com.mysql.cj.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/orm?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false&rewriteBatchedStatements=true";
        Connection connection = DriverManager.getConnection(url, "root", "123456");

        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        String now = sdf.format(new Date());

        int total = 10000;
        // 取消自动提交
        connection.setAutoCommit(false);
        for (int i = 1; i <= total; i++) {
            // 填充数据
            ps.setString(1, "test" + i);
            ps.setInt(2, 22);
            ps.setString(3, "test" + i + "@test.com");
            ps.setString(4, now);
            ps.setString(5, "system");
            ps.setString(6, now);
            ps.setString(7, "system");
            ps.setString(8, "N");
            // 将sql语句打包到一个容器中
            ps.addBatch();
            if (i % BATCH_SIZE == 0) {
                ps.executeBatch();
                ps.clearBatch();
            }
        }
        ps.executeBatch();
        ps.clearBatch();
        // 所有语句都执行完毕后才手动提交sql语句
        connection.commit();
        // 关闭
        ps.close();
        connection.close();
        System.out.println(total + "条数据插入用时：" + (System.currentTimeMillis() - start)+"毫秒");
    }

}
