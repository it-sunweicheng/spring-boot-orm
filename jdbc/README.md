# SpringBoot演示JDBC

> 此项目主要演示了如何使用 JDBC 操作数据库，包含简单使用测试示例。

## Connection对象
// 创建向数据库发送sql的statement对象。  
createStatement()  

// 创建向数据库发送预编译sql的PrepareStatement对象。  
prepareStatement(sql)  

// 创建执行存储过程的callableStatement对象  
prepareCall(sql)  

// 设置事务自动提交  
setAutoCommit(boolean autoCommit)  

// 提交事务  
commit()  

// 回滚事务  
rollback()  

## Statement对象
常用方法：  
// 查询（SELECT）  
executeQuery(String sql)  

// 增删改（INSERT INTO、UPDATE、DELETE）  
executeUpdate(String sql)  

// 任意sql语句都可以，但是目标不明确，很少用（DDL、DML）  
execute(String sql)  

// 把多条的sql语句放进同一个批处理中    
addBatch(String sql)    

// 向数据库发送一批sql语句执行    
executeBatch()    

// 清空批处理  
clearBatch()  

// 关闭
close()  

## PreparedStatement对象
常用方法：  
// 获取预编译sql语句（防止sql注入）  
prepareStatement(String sql)

// 查询（SELECT）  
executeQuery(String sql)

// 增删改（INSERT INTO、UPDATE、DELETE）  
executeUpdate(String sql)

// 任意sql语句都可以，但是目标不明确，很少用（DDL、DML）  
execute(String sql)  

// 把多条的sql语句放进同一个批处理中  
addBatch(String sql)  

// 向数据库发送一批sql语句执行    
executeBatch()  

// 清空批处理  
clearBatch()  

// 关闭  
close()  

## ResultSet对象
//获取任意类型的数据
getObject(String columnName)

//获取指定类型的数据【各种类型，查看API】
getString(String columnName)

//对结果集进行滚动查看的方法
next()

## 参考

- JDBC 官方文档：https://docs.oracle.com/javase/tutorial/jdbc/basics/index.html
