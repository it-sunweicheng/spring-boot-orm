-- 创建数据库
DROP DATABASE IF EXISTS `master`;
create schema master CHARSET utf8mb4  collate utf8mb4_general_ci;

-- 创建用户表
DROP TABLE IF EXISTS `master`.`user`;
CREATE TABLE `master`.`user` (
    `id` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `name` VARCHAR(30) DEFAULT NULL COMMENT '姓名',
    `age` INT(10) DEFAULT NULL COMMENT '年龄',
    `email` VARCHAR(50) DEFAULT NULL COMMENT '邮箱',
    `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `creator` VARCHAR(32) NOT NULL DEFAULT 'system' COMMENT '创建人',
    `modify_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    `modifier` VARCHAR(32) NOT NULL DEFAULT 'system' COMMENT '修改人',
    `is_deleted` CHAR(1) NOT NULL DEFAULT 'N' COMMENT 'N正常-Y删除',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8mb4 COMMENT='用户表';

-- 新增测试数据
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m官燕舞", "16", "guanyanwu@sun.com");
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m原涵菱", "17", "yuanlinhan@sun.com");
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m侨雪晴", "18", "qiaoxueqing@sun.com");
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m洋小宸", "16", "yangxiaochen@sun.com");
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m唐雨信", "17", "tangyuxin@sun.com");
INSERT INTO `master`.`user` (`name`, `age`, `email`) VALUES ("m司寇心", "18", "sikouxin@sun.com");


