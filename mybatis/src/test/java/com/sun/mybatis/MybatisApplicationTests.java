package com.sun.mybatis;

import com.alibaba.fastjson2.JSON;
import com.sun.mybatis.entity.User;
import com.sun.mybatis.mapper.UserMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
public class MybatisApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Test
    public void testSelect() {
        User user = userMapper.getUserById(1L);
        log.info("user = {}", user);
        assertThat(user.getName()).isEqualTo("官燕舞");
        assertThat(user.getAge()).isEqualTo(16);
    }

    @Test
    public void testInsert() {
        int count = userMapper.insert(User.builder()
                .name("澄心语")
                .age(20)
                .email("chengxinyu@sun.com")
                .creator("system")
                .modifier("system")
                .build());
        assertThat(count).isEqualTo(1);
    }

    /**
     * 通过foreach方式批量插入
     * 注意：如果超过mysql默认的最大入参量（mysql默认最大限制是4M(1024 * 1024 * 4)）
     * 查询最大限制 show VARIABLES like '%max_allowed_packet%';
     * 设置最大限制 set global max_allowed_packet = 1024 * 1024 * 500;
     *
     * 10000条数据插入用时：2490毫秒
     */
    @Test
    public void testBatchInsert() {
        long start = System.currentTimeMillis();
        List<User> users = new ArrayList<>();
        int total = 1000;
        for (int i = 0; i < total; i++) {
            users.add(User.builder()
                    .name("test" + i)
                    .age(19)
                    .email("test" + i + "@sun.com")
                    .build());
        }
        userMapper.batchInsert(users);
        log.info( "{}条数据插入用时：{}", total, (System.currentTimeMillis() - start) + "毫秒");
    }

    /**
     * BATCH批量模式
     * 注意：url需要添加rewriteBatchedStatements=true
     *
     * 10000条数据插入用时：1501毫秒
     */
    @Test
    public void testBatchInsertBatchMode() {
        long start = System.currentTimeMillis();
        // 开始BATCH批量模式
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH, false);
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);

        int total = 10000;
        for (int i = 0; i < total; i++) {
            mapper.insert(User.builder()
                    .name("test" + i)
                    .age(19)
                    .email("test" + i + "@sun.com")
                    .creator("system")
                    .modifier("system")
                    .build());
        }
        // 提交
        sqlSession.commit();
        // 关闭
        sqlSession.close();
        log.info( "{}条数据插入用时：{}", total, (System.currentTimeMillis() - start) + "毫秒");
    }

    @Test
    public void testBatchSelect() {
        List<Long> ids = new ArrayList<>();
        ids.add(1L);
        ids.add(2L);
        ids.add(3L);
        List<User> users = userMapper.batchQueryUser(ids);
        log.info("users = {}", JSON.toJSONString(users));
        assertThat(users.size()).isEqualTo(3);
    }

    @Test
    public void testBatchDelete() {
        List<Long> ids = new ArrayList<>();
        ids.add(7L);
        userMapper.batchDeleteUser(ids);
    }

}
