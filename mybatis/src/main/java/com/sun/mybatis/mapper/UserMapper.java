package com.sun.mybatis.mapper;

import com.sun.mybatis.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {

    int insert(User user);

    int delete(Long id);

    int update(User user);

    User getUserById(Long id);

    List<User> getAllUser();

    List<User> batchQueryUser(List<Long> ids);

    int batchDeleteUser(List<Long> ids);

    void batchInsert(List<User> users);

}
