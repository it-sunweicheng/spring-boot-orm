package com.sun.mybatis.service;

import com.sun.mybatis.entity.User;

import java.util.List;

public interface UserService {

    /**
     * 新增用户
     * @param user
     */
    void insertUser(User user);

    /**
     * 删除用户
     * @param id
     */
    void deleteUser(Long id);

    /**
     * 修改用户
     * @param user
     */
    void updateUser(User user);

    /**
     * 获取用户
     * @param id
     * @return
     */
    User getUserById(Long id);

    /**
     * 获取所有用户
     * @return
     */
    List<User> getAllUser();

}
