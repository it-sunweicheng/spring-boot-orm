package com.sun.mybatis.service.impl;

import com.sun.mybatis.entity.User;
import com.sun.mybatis.mapper.UserMapper;
import com.sun.mybatis.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;

    @Override
    @SneakyThrows
    public void insertUser(User user) {
        userMapper.insert(user);
    }

    @Override
    @SneakyThrows
    public void deleteUser(Long id) {
        userMapper.delete(id);
    }

    @Override
    @SneakyThrows
    public void updateUser(User user) {
        user.setModifyTime(new Date());
        userMapper.update(user);
    }

    @Override
    public User getUserById(Long id) {
        return userMapper.getUserById(id);
    }

    @Override
    public List<User> getAllUser() {
        return userMapper.getAllUser();
    }

}
