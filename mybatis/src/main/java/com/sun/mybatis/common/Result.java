package com.sun.mybatis.common;

import com.alibaba.fastjson2.JSON;
import lombok.Data;

import java.io.Serializable;

@Data
public class Result<T> implements Serializable {

    private static final long serialVersionUID = -1L;

    private Boolean           success          = false;

    private String            errorCode        = "";

    private String            errorMsg         = "";

    private T                 value;

    public Result() {
        super();
    }

    public Result(boolean success) {
        super();
        this.success = success;
    }

    public Result(T value) {
        super();
        this.success = true;
        this.value = value;
    }

    public Result(String errorCode, String errorMsg) {
        super();
        this.success = false;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public Result(boolean success, String errorCode, String errorMsg) {
        super();
        this.success = success;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    public static Result<Void> success() {
        Result<Void> result = new Result<>();
        result.setSuccess(true);
        return result;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

}
