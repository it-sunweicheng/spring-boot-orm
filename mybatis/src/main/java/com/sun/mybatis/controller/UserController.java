package com.sun.mybatis.controller;

import com.sun.mybatis.common.Result;
import com.sun.mybatis.entity.User;
import com.sun.mybatis.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    // http://localhost:8080/user/addUser
    @PostMapping("addUser")
    public Result<Void> addUser(@RequestBody User user) {
        userService.insertUser(user);
        return Result.success();
    }

    // http://localhost:8080/user/deleteUser?id=7
    @GetMapping("deleteUser")
    public Result<Void> deleteUser(@RequestParam("id") Long id) {
        userService.deleteUser(id);
        return Result.success();
    }

    // http://localhost:8080/user/modifyUser
    @PostMapping("modifyUser")
    public Result<Void> modifyUser(@RequestBody User user) {
        userService.updateUser(user);
        return Result.success();
    }

    // http://localhost:8080/user/getUser?id=1
    @GetMapping("getUser")
    public Result<User> getUser(@RequestParam("id") Long id) {
        return new Result<>(userService.getUserById(id));
    }

    // http://localhost:8080/user/getAllUser
    @GetMapping("getAllUser")
    public Result<List<User>> getAllUser() {
        return new Result<>(userService.getAllUser());
    }

}
