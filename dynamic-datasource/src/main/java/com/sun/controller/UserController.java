package com.sun.controller;

import com.sun.entity.User;
import com.sun.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    // http://localhost:8080/user/selectMasterUser?id=1
    @GetMapping("selectMasterUser")
    public User selectMasterUser(@RequestParam("id") Long id) {
        return userService.selectMasterUser(id);
    }

    // http://localhost:8080/user/selectSlaveUser?id=1
    @GetMapping("selectSlaveUser")
    public User selectSlaveUser(@RequestParam("id") Long id) {
        return userService.selectSlaveUser(id);
    }

}
