package com.sun.service;

import com.sun.entity.User;

/**
 * 用户接口
 * @author sun
 */
public interface UserService {

    User selectMasterUser(Long id);

    User selectSlaveUser(Long id);

}
