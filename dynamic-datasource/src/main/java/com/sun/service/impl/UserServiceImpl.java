package com.sun.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.sun.entity.User;
import com.sun.mapper.UserMapper;
import com.sun.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author sun
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @DS("master")
    @Override
    public User selectMasterUser(Long id) {
        return userMapper.selectById(id);
    }

    @DS("slave")
    @Override
    public User selectSlaveUser(Long id) {
        return userMapper.selectById(id);
    }

}
