package com.sun.mybatis.plus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sun.mybatis.plus.entity.User;
import com.sun.mybatis.plus.mapper.UserMapper;
import com.sun.mybatis.plus.service.UserBaseService;
import org.springframework.stereotype.Service;

/**
 * 通用实现
 */
@Service
public class UserBaseServiceImpl extends ServiceImpl<UserMapper, User> implements UserBaseService {
}
