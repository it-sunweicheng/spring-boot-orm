package com.sun.mybatis.plus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sun.mybatis.plus.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * 基础Mapper
 * @author sun
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
