package com.sun.mybatis.plus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sun.mybatis.plus.entity.User;

/**
 * 通用接口
 */
public interface UserBaseService extends IService<User> {
}
