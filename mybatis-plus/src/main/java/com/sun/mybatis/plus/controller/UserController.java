package com.sun.mybatis.plus.controller;

import com.sun.mybatis.plus.common.base.Result;
import com.sun.mybatis.plus.dto.UserDTO;
import com.sun.mybatis.plus.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author sun
 */
@Slf4j
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {

    private final UserService userService;

    // http://localhost:8080/user/addUser
    @PostMapping("addUser")
    public Result<Void> addUser(@RequestBody UserDTO user) {
        userService.insertUser(user);
        return Result.success();
    }

    // http://localhost:8080/user/deleteUser?id=1
    @GetMapping("deleteUser")
    public Result<Void> deleteUser(@RequestParam("id") Long id) {
        userService.deleteUser(id);
        return Result.success();
    }

    // http://localhost:8080/user/modifyUser
    @PostMapping("modifyUser")
    public Result<Void> modifyUser(@RequestBody UserDTO user) {
        userService.updateUser(user);
        return Result.success();
    }

    // http://localhost:8080/user/getUser?id=1
    @GetMapping("getUser")
    public Result<UserDTO> getUser(@RequestParam("id") Long id) {
        return new Result<>(userService.getUserById(id));
    }

}
