package com.sun.mybatis.plus;

import com.sun.mybatis.plus.entity.User;
import com.sun.mybatis.plus.mapper.UserMapper;
import com.sun.mybatis.plus.service.UserBaseService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
public class MybatisPlusApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserBaseService userBaseService;

    @Test
    public void testSelect() {
        User user = userMapper.selectById(1L);
        log.info("user = {}", user);
        assertThat(user.getName()).isEqualTo("官燕舞");
        assertThat(user.getAge()).isEqualTo(16);
    }

    @Test
    public void testBaseSelect() {
        User user = userBaseService.getById(1L);
        log.info("user = {}", user);
        assertThat(user.getName()).isEqualTo("官燕舞");
        assertThat(user.getAge()).isEqualTo(16);
    }

    @Test
    public void testBatchInsert() {
    }

}
