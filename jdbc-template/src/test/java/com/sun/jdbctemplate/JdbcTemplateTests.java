package com.sun.jdbctemplate;

import com.alibaba.fastjson2.JSON;
import com.sun.jdbctemplate.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
@SpringBootTest
public class JdbcTemplateTests {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /**
     * 查询单个对象
     */
    @Test
    public void testQuery() {
        String sql = "SELECT * FROM user WHERE id=?";
        User user = jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<>(User.class), 1L);
        log.info("user = {}", JSON.toJSONString(user));
    }

    /**
     * 查询单个对象
     */
    @Test
    public void testQueryList() {
        String sql = "SELECT * FROM user WHERE id>?";
        List<User> users = jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(User.class), 1L);
        log.info("users = {}", JSON.toJSONString(users));
    }

    /**
     * 插入单个对象
     */
    @Test
    public void testInsert() {
        Date now = new Date();
        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        int count = jdbcTemplate.update(sql,
                "易思琪",
                20,
                "yisiqi@sun.com",
                now,
                "system",
                now,
                "system",
                "N");
        log.info("insert count = {}", count);
    }

    /**
     * 批量插入
     * 注意：url需要加&rewriteBatchedStatements=true
     * 1000条数据插入用时：298毫秒
     * 1万条数据插入用时：641毫秒
     * 10万条数据插入用时：2126毫秒
     * 100万条数据插入用时：27935毫秒
     */
    @Test
    public void testBatchInsert() {
        long start = System.currentTimeMillis();
        String now = sdf.format(new Date());
        int batchSize = 1000000;
        List<Object[]> params = new ArrayList<>();
        for (int i = 0; i < batchSize; i++) {
            params.add(new Object[] { "易思琪" + i, 22, "yisiqi" + i + "@sun.com", now, "system", now, "system", "N"});
        }
        String sql = "INSERT INTO user(name, age, email, create_time, creator, modify_time, modifier, is_deleted ) " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.batchUpdate(sql, params);
        System.out.println(batchSize + "条数据插入用时：" + (System.currentTimeMillis() - start)+"毫秒");
    }

    /**
     * 更新
     */
    @Test
    public void testUpdate() {
        String sql = "UPDATE user SET age=?, email=? WHERE name = ?";
        int count = jdbcTemplate.update(sql,
                21,
                "yisiqi21@sun.com",
                "易思琪");
        log.info("update count = {}", count);
    }

    /**
     * 删除
     */
    @Test
    public void testDelete() {
        String sql = "DELETE FROM user WHERE name LIKE '%' ? '%'";
        int count = jdbcTemplate.update(sql, "易思琪");
        log.info("delete count = {}", count);
    }

}
