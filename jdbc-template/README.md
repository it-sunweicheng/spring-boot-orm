# SpringBoot整合JPA

> 本 demo 主要演示了Spring Boot如何使用 JdbcTemplate 操作数据库。

## 功能

- 查询、查询列表、新增、批量新增、更新、删除。

## 参考

- Spring Data JDBC 官方文档：https://spring.io/projects/spring-data-jdbc