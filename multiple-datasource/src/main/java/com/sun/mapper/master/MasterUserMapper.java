package com.sun.mapper.master;

import com.sun.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MasterUserMapper {

    int insert(User user);

    int delete(Long id);

    int update(User user);

    User getUserById(Long id);

    List<User> getAllUser();

}
