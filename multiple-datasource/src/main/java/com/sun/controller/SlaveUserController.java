package com.sun.controller;

import com.sun.entity.User;
import com.sun.mapper.master.MasterUserMapper;
import com.sun.mapper.slave.SlaveUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/slave")
public class SlaveUserController {

    @Resource
    private SlaveUserMapper slaveUserMapper;

    // http://localhost:8080/user/slave/addUser
    @PostMapping("addUser")
    public String addUser(@RequestBody User user) {
        slaveUserMapper.insert(user);
        return "OK";
    }

    // http://localhost:8080/user/slave/deleteUser?id=1
    @PostMapping("deleteUser")
    public String deleteUser(@RequestParam("id") Long id) {
        slaveUserMapper.delete(id);
        return "OK";
    }

    // http://localhost:8080/user/slave/modifyUser
    @PostMapping("modifyUser")
    public String modifyUser(@RequestBody User user) {
        slaveUserMapper.update(user);
        return "OK";
    }

    // http://localhost:8080/user/slave/getUser?id=1
    @GetMapping("getUser")
    public User getUser(@RequestParam("id") Long id) {
        return slaveUserMapper.getUserById(id);
    }

    // http://localhost:8080/user/slave/getAllUser
    @GetMapping("getAllUser")
    public List<User> getAllUser() {
        return slaveUserMapper.getAllUser();
    }

}
