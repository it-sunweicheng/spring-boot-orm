package com.sun.controller;

import com.sun.entity.User;
import com.sun.mapper.master.MasterUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/master")
public class MasterUserController {

    @Resource
    private MasterUserMapper masterUserMapper;

    // http://localhost:8080/user/master/addUser
    @PostMapping("addUser")
    public String addUser(@RequestBody User user) {
        masterUserMapper.insert(user);
        return "OK";
    }

    // http://localhost:8080/user/master/deleteUser?id=1
    @PostMapping("deleteUser")
    public String deleteUser(@RequestParam("id") Long id) {
        masterUserMapper.delete(id);
        return "OK";
    }

    // http://localhost:8080/user/master/modifyUser
    @PostMapping("modifyUser")
    public String modifyUser(@RequestBody User user) {
        masterUserMapper.update(user);
        return "OK";
    }

    // http://localhost:8080/user/master/getUser?id=1
    @GetMapping("getUser")
    public User getUser(@RequestParam("id") Long id) {
        return masterUserMapper.getUserById(id);
    }

    // http://localhost:8080/user/master/getAllUser
    @GetMapping("getAllUser")
    public List<User> getAllUser() {
        return masterUserMapper.getAllUser();
    }

}
