package com.sun.jpa;

import com.sun.jpa.entity.User;
import com.sun.jpa.repository.UserDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
@SpringBootTest
public class JpaApplicationTests {

    @Resource
    private UserDao userDao;

    @Test
    public void testSelect() {
        Optional<User> user = userDao.findById(1L);
        assertTrue(user.isPresent());

        log.info("user = {}", user.get());
        assertThat(user.get().getName()).isEqualTo("官燕舞");
        assertThat(user.get().getAge()).isEqualTo(16);
    }

    @Test
    public void testBatchInsert() {
        List<User> users = new ArrayList<>();
        int total = 10;
        LocalDateTime now = LocalDateTime.now();
        for (int i = 0; i < total; i++) {
            users.add(User.builder()
                    .name("test" + i)
                    .age(19)
                    .email("test" + i + "@sun.com")
                    .creator("jpa")
                    .createTime(now)
                    .modifier("jpa")
                    .modifyTime(now)
                    .isDeleted('N')
                    .build());
        }
        userDao.saveAll(users);
    }

}
