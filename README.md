# SpringBoot整合主流的ORM框架

## 项目简介

`spring-boot-orm` 是一个用来深度学习并实战 `ORM框架` 的项目，目前总共包含 **`7`** 个集成demo，已经完成 **`7`** 个。

该项目已成功集成 
JdbcTemplate(`通用JDBC操作数据库`)、 JPA(`强大的ORM框架`)、 MyBaits(`强大的ORM框架`)、 MyBatis-Plus(`快速操作Mybatis`)
、Dynamic Datasource(`快速操作Mybatis`)

## 开发环境

- **JDK 1.8 +**
- **Maven 3.5.x +**
- **Mysql 5.7 +**  
- **SpringBoot 2.7.x +**  
- **IntelliJ IDEA Ultimate 2022.2.x +**  

### Module 介绍

| Module 名称                                     | Module 介绍                        |
|-----------------------------------------------|----------------------------------|
| [dynamic-datasource](./dynamic-datasource)    | SpringBoot 整合 Dynamic Datasource |
| [jdbc](./jdbc)                                | SpringBoot 整合 Jdbc API           |
| [jdbc-template](./jdbc-template)              | SpringBoot 整合 Jdbc Template      |
| [jpa](./jpa)                                  | SpringBoot 整合 JPA                |
| [multiple-datasource](./multiple-datasource)  | SpringBoot 整合 多数据源               |
| [mybatis](./mybatis)                          | SpringBoot 整合 MyBatis            |
| [mybatis-plus](./mybatis-plus)                | SpringBoot 整合 MyBatis Plus       |
